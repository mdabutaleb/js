<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>js object</title>
</head>
<body>
<p id="demo"></p>
<?php
$a = ['A', 'B', 'C'];
$b = ['X' => $a, 'Y', 'Z'];

echo "<pre>";
 json_encode($b);
echo "</pre>";
?>
<script type="text/javascript">
    var myvalue = <?php echo json_encode($b)?>;
    console.log(myvalue);
</script>
</body>
</html>