<?php
$fistname = $_REQUEST['first_name'];
$lastname = $_REQUEST['last_name'];
$dbh = new PDO('mysql:host=localhost;dbname=ajax', 'root', '');
$query = "INSERT INTO USERS (id, first_name, last_name) VALUE (:i, :f, :l)";
$stmt = $dbh->prepare($query);
$output = $stmt->execute(
    array(
        ':i' => null,
        ':f' => $fistname,
        ':l' => $lastname,
    )
);
if ($output) {
    echo "ok";
}