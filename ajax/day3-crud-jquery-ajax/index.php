<html>
<head>
    <title>Ajax Crud</title>
    <script src="jquery/jquery-3.1.1.js"></script>
</head>
<body>
<a href="javascript:void(0);" onclick="javascript:$('addForm')">Add new</a>
<div id="addForm">
    <form id="MyAddForm">
        <input type="text" name="first_name" id="first_name" placeholder="First name" autofocus/>
        <input type="text" name="last_name" id="last_name" placeholder="Last Name"/>
        <input type="button" name="btn" value="Save" onclick="userAction('add')"/>
    </form>
</div>
<div id="output">

</div>
<script>
    function userAction(type, id) {
        var id = (typeof id == 'undefined') ? '' : id;
        if (type == 'add') {

            var UserData = $("#addForm").find("#MyAddForm").serialize() + '&action_type=' + type + '&id=' + id;
//            console.log(UserData);
        }
        $.ajax(
            {
                type: 'POST',
                url: 'userAction.php',
                data: UserData,
                success: function (msg) {
                    if (msg == 'ok') {
                        alert('User Data Has been saved successfully');
                    }else {
                        alert('oops something going wrong ~')
                    }

                }
            }
        )

    }
</script>
</body>
</html>