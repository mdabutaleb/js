<?php
// Array with names
$dbh = new PDO('mysql:host=localhost;dbname=sumonmhd_my', 'root', '');

$st = $dbh->prepare("select name from users");
$st->execute();
$a = $st->fetchAll(PDO::FETCH_ASSOC);
$t = [];
foreach ($a as $item) {
    $t[] = $item['name'];
}

$q = $_REQUEST["q"];
$value = "";
$length = strlen($q);
foreach ($t as $item) {
    if (stristr($q, substr($item, 0, $length))) {
        $value .= "$item , ";
    }
}
if (!empty($value)) {
    echo $value;
} else {
    echo "Not found";
}
