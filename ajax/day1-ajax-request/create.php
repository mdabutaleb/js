<html>
<head>
    <title>Ajax Request</title>
</head>
<body>
<form>
    <label>Enter Your Name:</label>
    <input type="text" onkeyup="showHint(this.value)"/>

</form>
<div style="background-color: gainsboro; width: 400px; height: 300px; padding: 20px" id="output">
    default
</div>
<script>
    function showHint(str) {
        if (str.length == 0) {
            document.getElementById("output").innerHTML = "";
            return;
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("output").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "data.php?q=" + str, true);
            xmlhttp.send();
        }
    }
</script>
</body>
</html>