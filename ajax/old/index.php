<html>
<head>
    <title>
        AJAX CRUD OPERATION
    </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript">
        function userAction(type, id) {
            id = (typeof id == "undefined") ? '' : id;
            var statusArr = {add: "added", edit: "updated", delete: "deleted"};
            var userData = '';
            if (type == 'add') {
                userData = $("#addForm").find('.form').serialize() + '&action_type=' + type + '&id=' + id;
            }
            $.ajax({
                type: 'POST',
                url: 'userAction.php',
                data: userData,
                success: function (msg) {
                    if (msg == 'ok') {
                        alert('User data has been ' + statusArr[type] + ' successfully.');
                        getUsers();
                        $('.form')[0].reset();
                        $('.formData').slideUp();
                    } else {
                        alert('Some problem occurred, please try again.');
                    }
                }
            });
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="panel panel-default users-content">
            <div class="panel-heading">Users <a href="javascript:void(0);" class="glyphicon glyphicon-plus" id="addLink"
                                                onclick="javascript:$('#addForm').slideToggle();">Add</a></div>
            <div class="panel-body none formData" id="addForm">
                <h2 id="actionLabel">Add User</h2>
                <form class="form" id="userForm">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" id="name"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" id="email"/>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" name="phone" id="phone"/>
                    </div>
                    <a href="javascript:void(0);" class="btn btn-warning" onclick="$('#addForm').slideUp();">Cancel</a>
                    <a href="javascript:void(0);" class="btn btn-success" onclick="userAction('add')">Add User</a>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>