<?php
include 'db.php';
$db = new DB();
$tblName = 'users';
if (isset($_POST['action_type']) && !empty($_POST['action_type'])) {
    if ($_POST['action_type'] == 'add') {
        $db->setData($_POST);
        $db->store();
        echo $db ? 'ok' : 'err';
    }
    exit;
}