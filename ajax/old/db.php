<?php

class DB
{
    public $id = '';
    public $title = '';
    public $conn = '';
    public $dbuser = 'root';
    public $dbpass = '';
    public $order = '';
    public $page_number = '';
    public $item_per_pg = '';

    public function __construct()
    {
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=pondit", $this->dbuser, $this->dbpass);

    }

    public function setData($data)
    {
        if (array_key_exists('mobile_model', $data) && !empty($data['mobile_model'])) {
            $this->title = $data['mobile_model'];
        }

        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (array_key_exists('order', $data) && !empty($data['order'])) {
            $this->order = $data['order'];
        }
        if (array_key_exists('page', $data) && !empty($data['page'])) {
            $this->page_number = $data['page'];
        }

        if (array_key_exists('item_p_pg', $data) && !empty($data['item_p_pg'])) {
            $this->item_per_pg = $data['item_p_pg'];
        }
        return $this;

    }


    public function getRows($table, $conditions = array())
    {
        try {
            if ($this->order == 'a-z') {
//                $query = "SELECT * FROM mobile_models ORDER by title LIMIT $this->item_per_pg OFFSET " . $this->page_number * $this->item_per_pg." WHERE is_delete=1";
                $query = "SELECT * FROM mobile_models WHERE is_delete=0 ORDER BY title LIMIT $this->item_per_pg  OFFSET " . $this->page_number * $this->item_per_pg;
            } else {
                $query = "SELECT * FROM mobile_models WHERE is_delete=0 ORDER BY id DESC LIMIT $this->item_per_pg  OFFSET " . $this->page_number * $this->item_per_pg;

            }
            $stmt = $this->conn->query($query);
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function store()
    {
        try {
            $query = "INSERT INTO mobile_models(id, title) VALUES(:id, :title)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':id' => null,
                ':title' => $this->title,
            ));

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }


    public function update($table, $data, $conditions)
    {
        if (!empty($data) && is_array($data)) {
            $colvalSet = '';
            $whereSql = '';
            $i = 0;
            if (!array_key_exists('modified', $data)) {
                $data['modified'] = date("Y-m-d H:i:s");
            }
            foreach ($data as $key => $val) {
                $pre = ($i > 0) ? ', ' : '';
                $colvalSet .= $pre . $key . "='" . $val . "'";
                $i++;
            }
            if (!empty($conditions) && is_array($conditions)) {
                $whereSql .= ' WHERE ';
                $i = 0;
                foreach ($conditions as $key => $value) {
                    $pre = ($i > 0) ? ' AND ' : '';
                    $whereSql .= $pre . $key . " = '" . $value . "'";
                    $i++;
                }
            }
            $query = "UPDATE " . $table . " SET " . $colvalSet . $whereSql;
            $update = $this->db->query($query);
            return $update ? $this->db->affected_rows : false;
        } else {
            return false;
        }
    }

    /*
    * Delete data from the database
    * @param string name of the table
    * @param array where condition on deleting data
    */
    public function delete($table, $conditions)
    {
        $whereSql = '';
        if (!empty($conditions) && is_array($conditions)) {
            $whereSql .= ' WHERE ';
            $i = 0;
            foreach ($conditions as $key => $value) {
                $pre = ($i > 0) ? ' AND ' : '';
                $whereSql .= $pre . $key . " = '" . $value . "'";
                $i++;
            }
        }
        $query = "DELETE FROM " . $table . $whereSql;
        $delete = $this->db->query($query);
        return $delete ? true : false;
    }
}